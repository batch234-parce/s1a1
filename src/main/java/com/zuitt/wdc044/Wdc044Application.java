package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// annotation-under the hood codes to be used
@RestController
// handles the endpoint for web requests
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}
	// @RequestParam- annotation tells Spring to expect a name value in the request but if it's not there, a default "World will be used instead
	@GetMapping("/hello")
	// @GetMapping tells Spring to use this method when a GET request is received
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){

		return String.format("Hello %s", name);
		// %s- any type but the return is a String
		// %c- character and the output is a unicode character
		// %b- any type but the output is boolean
	}

	@GetMapping("/favoriteFood")
	public String favoriteFood(@RequestParam(value = "food", defaultValue = "rice") String food){
		return String.format("Hi! My favorite food is %s.", food);
	}

	@GetMapping("/greeting")
	public String greeting(@RequestParam(value = "timeOfDay", defaultValue = "morning") String timeOfDay, @RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("Good %s, %s!", timeOfDay, user);
	}
}
// command for running the application
// ./mvnw spring-boot:run
// route for checking: localhost:8080/hello
// with parameter: localhost:8080/hello?parameter=value
