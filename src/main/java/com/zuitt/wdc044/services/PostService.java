package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // Create a post
    void createPost(String stringToken, Post post);
    // Retrieve posts

    ResponseEntity updatePost(Long id, String stringToken, Post post);
    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id, String stringToken);

    // Activity s5
    Iterable<Post> getMyPosts(String stringToken);

}
